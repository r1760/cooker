package com.karpiuk.common_ui.theme

import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.ui.graphics.Color

val DarkColorScheme = darkColorScheme(
    primary = Color(0xFF93ccff),
    onPrimary = Color(0xFF003351),
    primaryContainer = Color(0xFF004b74),
    onPrimaryContainer = Color(0xFFcde5ff),
    secondary = Color(0xFF94ccff),
    onSecondary = Color(0xFF003352),
    secondaryContainer = Color(0xFF004b74),
    onSecondaryContainer = Color(0xFFcde5ff),
    tertiary = Color(0xFF94d785),
    onTertiary = Color(0xFF003a02),
    tertiaryContainer = Color(0xFF135210),
    onTertiaryContainer = Color(0xFFaff49f),
    background = Color(0xFF1a1c1e),
    onBackground = Color(0xFFe2e2e5),
    surface = Color(0xFF1a1c1e),
    onSurface = Color(0xFFe2e2e5),
)

val LightColorScheme = lightColorScheme(
    primary = Color(0xFF006398),
    onPrimary = Color(0xFFffffff),
    primaryContainer = Color(0xFFcde5ff),
    onPrimaryContainer = Color(0xFF001d32),
    secondary = Color(0xFF006399),
    onSecondary = Color(0xFFffffff),
    secondaryContainer = Color(0xFFcde5ff),
    onSecondaryContainer = Color(0xFF001d32),
    tertiary = Color(0xFF2e6b27),
    onTertiary = Color(0xFFffffff),
    tertiaryContainer = Color(0xFFaff49f),
    onTertiaryContainer = Color(0xFF102004),
    background = Color(0xFFefffff),
    onBackground = Color(0xFF1a1c1e),
    surface = Color(0xFFefefff),
    onSurface = Color(0xFF1a1c1e),
)