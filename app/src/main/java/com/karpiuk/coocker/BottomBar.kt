package com.karpiuk.coocker

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.layout.size
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.karpiuk.core.R

@Composable
fun BottomBar(
    modifier: Modifier = Modifier,
    navController: NavController,
) {

    var navigationScreenSelected: Screen by remember { mutableStateOf(Screen.START_SCREEN) }

    NavigationBar(
        modifier = modifier,
    ) {
        BottomBarItem.values().forEach { bottomBarItem ->
            NavigationBarItem(
                selected = navigationScreenSelected == bottomBarItem.screen,
                label = { Text(stringResource(bottomBarItem.titleResId)) },
                icon = {
                    Icon(
                        modifier = Modifier.size(24.dp),
                        painter = painterResource(id = bottomBarItem.iconId),
                        contentDescription = stringResource(bottomBarItem.titleResId)
                    )
                },
                onClick = {
                    navigationScreenSelected = bottomBarItem.screen
                    navController.navigate(bottomBarItem.screen.route) {
                        navController.graph.startDestinationRoute?.let { route ->
                            popUpTo(route) {
                                saveState = true
                            }
                        }

                        launchSingleTop = true
                        restoreState = true
                    }
                }
            )
        }
    }
}

private enum class BottomBarItem(
    @DrawableRes val iconId: Int,
    @StringRes val titleResId: Int,
    val screen: Screen,
) {
    Recipes(
        iconId = R.drawable.ic_book,
        titleResId = R.string.recipes,
        screen = Screen.RecipesScreen,
    ),
    Calories(
        iconId = R.drawable.ic_scheduler,
        titleResId = R.string.calories,
        screen = Screen.CaloriesScreen,
    ),
    Profile(
        iconId = R.drawable.ic_profile,
        titleResId = R.string.profile,
        screen = Screen.ProfileScreen,
    ),
}