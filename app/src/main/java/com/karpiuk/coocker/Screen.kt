package com.karpiuk.coocker

internal sealed class Screen(val route: String) {

    companion object {
        internal val START_SCREEN = CaloriesScreen
    }

    object RecipesScreen : Screen("recipes_screen")
    object CaloriesScreen : Screen("calories_screen")
    object ProfileScreen : Screen("profile_screen")
}